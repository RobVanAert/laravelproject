<?php


namespace App\Domain;

use Doctrine\Common\Collections\ArrayCollection;


class Task
{

    private $id;
    private $title;
    private $description;
    private $completed = false;
    private $remarksCompleted;
    private $week;
    private $user;


    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param mixed $completed
     */
    public function setCompleted($completed): void
    {
        $this->completed = $completed;
    }

    /**
     * @return mixed
     */
    public function getRemarksCompleted()
    {
        return $this->remarksCompleted;
    }

    /**
     * @param mixed $remarksCompleted
     */
    public function setRemarksCompleted($remarksCompleted): void
    {
        $this->remarksCompleted = $remarksCompleted;
    }

    /**
     * @return mixed
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * @param mixed $week
     */
    public function setWeek(WorkWeek $week): void
    {
        $week->setTasks($this);
        $this->week = $week;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(User $user): void
    {
        $user->setTasks($this);
        $this->user = $user;
    }
}