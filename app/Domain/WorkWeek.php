<?php


namespace App\Domain;


use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

class WorkWeek
{
    private $id;
    private $weekNumber;
    private $startDate;
    private $endDate;
    private $tasks;

    /**
     * WorkWeek constructor.
     * @param $weekNumber
     * @param $startDate
     * @param $endDate
     */
    public function __construct()
    {
        $this->tasks=new ArrayCollection;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getWeekNumber()
    {
        return $this->weekNumber;
    }

    /**
     * @param mixed $weekNumber
     */
    public function setWeekNumber($weekNumber): void
    {
        $this->weekNumber = $weekNumber;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate->format('j F Y');
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate): void
    {
        $this->startDate = new DateTime($startDate);
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate->format('j F Y');
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate): void
    {
        $this->endDate = new DateTime($endDate);
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks): void
    {
        $this->tasks[] = $tasks;
    }


}