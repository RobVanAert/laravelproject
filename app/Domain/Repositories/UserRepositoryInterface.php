<?php

namespace App\Domain\Repositories;

interface UserRepositoryInterface
{
    public function create(array $input);

    public function resetPassword ($user, $password);
}