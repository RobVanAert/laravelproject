<?php


namespace App\Domain\Repositories;


use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use App\Domain\WorkWeek;


class WorkWeekRepository extends EntityRepository implements WorkWeekRepositoryInterface
{
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    public function getAllWeeks()
    {
        return $this->findAll();
    }

    public function  getWeekById ($id)
    {
        return $this->find($id);
    }

    public function createWeek($week)
    {
        $newWeek= new workWeek;
        $newWeek->setWeekNumber($week ['weekNumber']);
        $newWeek->setStartDate($week['startDate']);
        $newWeek->setEndDate($week['endDate']);

        $this->_em->persist($newWeek);
        $this->_em->flush();
        return ($newWeek->getId());
    }

    public function updateWeek($id, $week)
    {
        // TODO: Implement updateWeek() method.
    }

    public function deleteWeek($id)
    {
        // TODO: Implement deleteWeek() method.
    }
}