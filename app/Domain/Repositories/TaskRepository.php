<?php


namespace App\Domain\Repositories;


use App\Domain\Task;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;


class TaskRepository extends EntityRepository implements TaskRepositoryInterface
{
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    public function createTask ($input)
    {
        $task=new task;
        $task->setTitle($input['title']);
        $task->setDescription($input['description']);
        $task->setWeek($input['week']);
        $task->setUser($input['user']);

        $this->_em->persist($task);
        $this->_em->flush();

    }

    public function updateTask ($id, $taskIn)
    {
        $task= $this->getTaskById($id);

        $task->SetTitle($taskIn ['title']);
        $task->SetDescription($taskIn['description']);
        if (array_key_exists('completed', $taskIn))
        $task->SetCompleted($taskIn['completed']);
        if (array_key_exists('remarks', $taskIn))
        $task->SetRemarksCompleted($taskIn['remarks']);

        $this->_em->flush();
    }

    public function deleteTask($id)
    {
        $task=$this->getTaskById($id);

        $this->_em->remove($task);
        $this->_em->flush();
    }

    public function getAllTasks()
    {
        return $this->findAll();
    }

    public function getTaskById($Id)
    {
        return $this->find($Id);
    }

    public function getTaskByUser($user)
    {
        return $this->findBy(['user'=>$user]);
    }
}