<?php

namespace App\Domain\Repositories;

interface WorkWeekRepositoryInterface
{
    public function createWeek($week);
    public function getAllWeeks();
    public function updateWeek($id, $week);
    public function deleteWeek($id);
    public function getWeekById($id);
}