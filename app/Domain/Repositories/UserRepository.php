<?php


namespace App\Domain\Repositories;


use App\Domain\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
Use App\Services\PasswordService;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    public function create(array $input)
    {
        $hasher=new PasswordService();
        $hashedPassword=$hasher->hashPassword($input ['password']);

        $user=new User();
        $user->setEmail($input['email']);
        $user->setUserName($input['name']);
        $user->setPassword($hashedPassword);

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    public function resetPassword($user, $password)
    {
        $hasher=new PasswordService();
        $hashedPassword=$hasher->hashPassword($password);

        $userDB=$this->find($user->getId());
        $userDB->setPassword($hashedPassword);
        $userDB->setRememberToken (Str::random(60));

        $this->_em->flush();
    }
}