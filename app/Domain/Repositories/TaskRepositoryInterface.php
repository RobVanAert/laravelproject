<?php

namespace App\Domain\Repositories;

interface TaskRepositoryInterface
{
    public function createTask($task);
    public function updateTask($id, $task);
    public function deleteTask($id);
    public function getAllTasks();
    public function getTaskById($id);
    public function getTaskByUser ($user);


}