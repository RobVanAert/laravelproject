<?php


namespace App\Services;

use Illuminate\Support\Facades\Hash;

class PasswordService
{
    public function __construct()
    {
    }

    public function hashPassword($password)
    {
        return Hash::make($password);
    }
}