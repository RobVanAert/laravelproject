<?php

namespace App\Providers;

//use Doctrine\ORM\EntityManager;
use App\Domain\Repositories\UserRepository;
use App\Domain\Repositories\UserRepositoryInterface;
use App\Domain\Task;
use App\Domain\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\Domain\Repositories\TaskRepositoryInterface;
use App\Domain\Repositories\TaskRepository;
use App\Domain\WorkWeek;
use App\Domain\Repositories\WorkWeekRepository;
use App\Domain\Repositories\WorkWeekRepositoryInterface;
use LaravelDoctrine\ORM\Facades\EntityManager;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            TaskRepositoryInterface::class, function ($app){
                return new TaskRepository(
                    $app['em'],
                    $app['em']->getClassMetaData(Task::class)
                );
        });

        $this->app->bind(
            WorkWeekRepositoryInterface::class, function ($app){
            return new WorkWeekRepository(
                $app['em'],
                $app['em']->getClassMetaData(WorkWeek::class)
            );
        });

        $this->app->bind(
            UserRepositoryInterface::class, function ($app){
                return new UserRepository(
                    $app['em'],
                    $app['em']->getClassMetaData(User::class)
                );
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
