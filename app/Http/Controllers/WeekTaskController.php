<?php

namespace App\Http\Controllers;

use App\Domain\Repositories\TaskRepositoryInterface;
use App\Domain\Repositories\WorkWeekRepositoryInterface;
use App\Domain\WorkWeek;
use Illuminate\Http\Request;

class WeekTaskController extends Controller
{
    private $taskRepository;
    private $weekRepository;

    public function __construct(
        TaskRepositoryInterface $taskRepository,
        WorkWeekRepositoryInterface $workWeekRepository)
    {
        $this->taskRepository=$taskRepository;
        $this->weekRepository=$workWeekRepository;
        $this->middleware('auth');
    }

    public function store($weekId)
    {
        $user= auth()->user();

        $input=request()->validate([
            'title' =>['required', 'min:3'],
            'description' =>['required', 'min:10']
        ]);
        $input['week']=$this->weekRepository->getWeekById($weekId);
        $input['user']=$user;

        $this->taskRepository->createTask($input);

        return redirect(route('week.show',[$id=$weekId]));
    }
}
