<?php

namespace App\Http\Controllers\Auth;

use App\Domain\Repositories\UserRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    private $userRepository;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository=$userRepository;
        $this->middleware('guest');
    }

    public function resetPassword($user, $password)
    {
        $this->userRepository->resetPassword($user,$password);
        $this->guard()->login($user);
    }
}
