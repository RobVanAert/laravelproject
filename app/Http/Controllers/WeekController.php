<?php

namespace App\Http\Controllers;

use App\Domain\Repositories\WorkWeekRepositoryInterface;
use Illuminate\Http\Request;


class WeekController extends Controller
{
    private $WorkWeekRepository;

    public function __construct(WorkWeekRepositoryInterface $WorkWeekRepository)
    {
        $this->WorkWeekRepository=$WorkWeekRepository;
        $this->middleware('auth')->only(['show']);
    }

    public function index()
    {
        $weeks=$this->WorkWeekRepository->getAllWeeks();

        return view('Weeks/index', compact('weeks'));
    }

    public function create()
    {
        return view('Weeks/create');
    }

    public function show ($id)
    {

        $week=$this->WorkWeekRepository->getWeekById($id);

        //dd($week->getTasks());

        return view('Weeks/show', compact('week'));
    }

    public function store ()
    {
        $input=request()->validate([
            'weekNumber'=>['required', 'integer'],
            'startDate' =>['required', 'date'],
            'endDate'=>['required', 'date']
            ]);

        $weekId= $this->WorkWeekRepository->createWeek($input);

        return redirect(route('week.show',['$id'=> $weekId]));
    }
}
