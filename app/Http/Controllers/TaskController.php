<?php

namespace App\Http\Controllers;


use App\Domain\Repositories\TaskRepositoryInterface;
use Illuminate\Http\Request;
use App\Domain\Task;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    private $taskRepository;

    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->middleware('auth');
    }

    public function index ()
    {
        $tasks = $this->taskRepository->getAllTasks();
        return view ('Tasks/index', compact('tasks'));
    }

    public function create()
    {
        return view ('Tasks/create');
    }

    public function store()
    {

       $input= $this->validateTask();

        $this->taskRepository->createTask($input);

        return redirect(route('week/{$id}'));
    }

    public function edit($id)
    {
        $task= $this->taskRepository->getTaskById($id);

        abort_if(auth()->user() !== $task->getUser(),403);

        return view('Tasks/edit', compact('task'));
    }

    public function update ($id)
    {
        $input= $this->validateTask();
        $input['completed'] = (request()->has('completed'));
        $input['remarks']= request('remarks');
        $this->taskRepository->updateTask($id, $input);

        $weekId=$this->taskRepository->getTaskById($id)->getWeek()->getId();
        return redirect (route('week.show',['$id'=>$weekId]));
    }

    public function delete ($id)
    {
        $weekId=$this->taskRepository->getTaskById($id)->getWeek()->getId();
        $this->taskRepository->deleteTask($id);

        return redirect (route('week.show',['$id'=>$weekId]));
    }

    private function validateTask()
    {
        return request()->validate([
            'title' =>['required', 'min:3'],
            'description' =>['required', 'min:10'],
            'remarks' => ['nullable', 'min:10']
        ]);
    }

}

