<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses'=>'PagesController@home',
    'as'=>'home']);
Route::get('/about', [
    'uses'=>'PagesController@about',
    'as'=>'about']);
Route::get('/contact',[
    'uses'=>'PagesController@contact',
    'as'=> 'contact']);

Route::group(['prefix'=>'task','as'=>'task'],function() {
    Route::get('', [
        'uses' => 'TaskController@index',
        'as' => '']);
    Route::post('', [
        'uses' => 'TaskController@store',
        'as' => '.store']);
    Route::get('/create', [
        'uses' => 'TaskController@create',
        'as' => '.create']);
    Route::get('/{task}/edit', [
        'uses' => 'TaskController@edit',
        'as' => '.edit']);
    Route::patch('/{task}', [
        'uses' => 'TaskController@update',
        'as' => '.update']);
    Route::delete('/{task}', [
        'uses' => 'TaskController@delete',
        'as' => '.delete']);
});

Route::group(['prefix'=>'week', 'as'=>'week.'], function(){

    Route::get('/',[
        'uses'=>'WeekController@index',
        'as'=>'']);
    Route::post('/',[
        'uses'=>'WeekController@store',
        'as'=>'store']);
    Route::get('/create',[
        'uses'=>'WeekController@create',
        'as'=>'create']);
    Route::get('/{week}/edit',[
        'uses'=>'weekController@edit',
        'as'=>'edit']);
    Route::get('/{week}',[
        'uses'=>'WeekController@show',
        'as'=>'show']);
});

Route::group(['prefix'=>'week/{week}/task', 'as'=>"weekTask"], function(){

    Route::post('/',[
        'uses'=>'WeekTaskController@store',
        'as'=> '']);
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
