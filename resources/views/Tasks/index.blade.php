@extends('partials.layout')

@section('title')
    Tasks
@endsection

@section ('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 ">
                <table class=" table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th class="w-25">Title</th>
                            <th class="w-100">Description</th>
                            <th>Completed</th>
                            <th>Remarks</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{$task->getTitle()}}</td>
                                <td>{{$task->getDescription()}}</td>
                                <td>{{($task->getCompleted())?'Yes':'No'}}</td>
                                <td>{{$task->getRemarksCompleted()}}</td>
                                <td>
                                    <form method="get" action="{{route('task.edit',['id'=>$task->getId()])}}" >
                                        <button type="submit" class="btn btn-success btn-block">Edit task</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div>
                <form method="GET" action="{{route('task.create')}}">
                    <button type="submit" class="btn btn-success">Create new task</button>
                </form>

            </div>
        </div>
    </div>
@endsection