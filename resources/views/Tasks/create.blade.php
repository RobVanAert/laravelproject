@extends('partials/layout')

@section('title')
    Create task
@endsection

@section ('content')
<div class="container" >
    <div class="row">
        <div class="col-lg-2">
            <form method="post" action="{{route('task.store')}}">
                @csrf
                <div class="form-group">
                    <label >Title </label>
                    <input type="text" name="title" class="form-control" required>
                </div>
                <div class="form-group">
                    <label >Description </label>
                    <input type="text" name="description" class="form-control" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Create task</button>
                </div>
                @include('partials.errors')
            </form>
        </div>
    </div>
</div>

@endsection