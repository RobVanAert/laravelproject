@extends ('partials.layout')

@section('title')
   Edit task
@endsection

@section ('content')
    <div class="container" >
        <div class="row">
            <div class="col-lg-6">
                <form method="post" action="{{route('task.update',['id'=>$task->getId()])}}">
                    @csrf
                    @method("patch")
                    <div class="form-group">
                        <label >Title </label>
                        <input type="text" name="title" class="form-control"value="{{$task->getTitle()}}" required >
                    </div>
                    <div class="form-group">
                        <label >Description </label>
                        <textarea type="text" name="description" class="form-control" required>{{$task->getDescription()}}  </textarea>
                    </div>
                    <div class="form-group">
                        <label class="custom-checkbox" for="completed">Completed </label>
                        <input type="checkbox" name="completed" {{$task->getCompleted()? 'checked':'' }} >
                    </div>
                        <div class="form-group">
                            <label class="form-group">Remarks when completed</label>
                            <textarea type="text" name="remarks" class="form-control" {{$task->getRemarksCompleted()}}> </textarea>
                        </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Update task</button>
                    </div>
                </form>
                <form method = "post" action="{{route('task.delete',['id'=>$task->getId()])}}">
                    @csrf
                    @method('delete')
                    <div class="form-group">
                        <button type="submit" class="btn btn-dark">Delete task</button>
                    </div>
                     @include('partials.errors')
                </form>
            </div>
        </div>
    </div>


@endsection
