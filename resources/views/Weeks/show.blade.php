@extends ('partials.layout')

@section('title')
    Week/{{$week->getWeekNumber()}}
@endsection

@section ('content')
    <div class="container mt-3">
        <div class="row">
            <div class="col-lg-7">
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row">Datum</th>
                        <td>Week {{$week->getWeekNumber()}} van {{$week->getStartDate()}} t.e.m. {{$week->getEndDate()}}</td>
                    </tr>
                    <tr>
                        <th scope="row">geplande taken</th>
                        <td>
                            <ul>
                                @foreach($week->getTasks() as $task)
                                    @if($task->getUser() === \Illuminate\Support\Facades\Auth::user())
                                       <li>
                                           <a href="{{route('task.edit',['id'=>$task->getId()])}}">{{$task->getTitle()}}</a>
                                       </li>
                                    @endif
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Voltooide taken</th>
                        <td>
                        <ul>
                            @foreach($week->getTasks() as $task)
                                @if($task->getCompleted() == 1 && $task->getUser() === \Illuminate\Support\Facades\Auth::user())
                                <li>
                                    {{$task->getTitle()}}
                                    <br>
                                    <span class="pl-3">{{$task->getRemarksCompleted()}}</span>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4 offset-lg-1">
                <h4 class="ml-3">Create a new task for this week</h4>
                <div class="col-lg-10 col-md-6">
                    <form method="post" action="{{route('weekTask',[$id=$week->getId()])}}">
                        @csrf
                        <div class="form-group">
                            <label >Title </label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label >Description </label>
                            <textarea type="text" name="description" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Create task</button>
                        </div>
                        @include('partials.errors')
                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection
