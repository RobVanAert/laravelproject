@extends ('partials.layout')

@section('title')
    Week
@endsection

@section('content')
    <div class="container mt-3">
        <div class="row">
            <div class="col-lg-8 col-md-12 ">
                <table class=" table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th>Week number</th>
                        <th>Start date</th>
                        <th>End date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($weeks as $week)
                        <tr>
                            <td class="text-center">{{$week->getWeekNumber()}}</td>
                            <td>{{$week->getStartDate()}}</td>
                            <td>{{$week->getEndDate()}}</td>
                            <td>
                                <form method="get" action="{{route('week.show',['id'=>$week->getId()])}}" >
                                    <button type="submit" class="btn btn-success btn-block">See tasks</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <form method="get" action="{{route('week.create')}}">
                <button type="submit" class="btn btn-success">New week</button>
            </form>
        </div>
    </div>
@endsection