@extends ('partials.layout')

@section('title')
    New week
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 ">
               <form method = "post" action="{{route("week.store")}}">
                   @csrf
                   <div class="form-group">
                       <label>Week number </label>
                       <input type="text" name="weekNumber" class="form-control" value="{{old('weekNumber')}}">
                   </div>
                   <div class="form-group">
                       <label>Start date </label>
                       <input type="date" name="startDate" class="form-control" value="{{old('startDate')}}">
                   </div>
                   <div class="form-group">
                       <label>End date </label>
                       <input type="date" name="endDate" class="form-control" value="{{old('endDate')}}">
                   </div>
                   <div class="form-group">
                       <button type="submit" class="btn-success">Save Week</button>
                   </div>
                   @include('partials.errors')
               </form>
            </div>
        </div>
    </div>
@endsection