<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190516114815 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Tasks (id INT AUTO_INCREMENT NOT NULL, week_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, completed TINYINT(1) NOT NULL, remarks_completed VARCHAR(255) DEFAULT NULL, INDEX IDX_91994A93C86F3B2F (week_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE WorkWeeks (id INT AUTO_INCREMENT NOT NULL, week_number INT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Tasks ADD CONSTRAINT FK_91994A93C86F3B2F FOREIGN KEY (week_id) REFERENCES WorkWeeks (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Tasks DROP FOREIGN KEY FK_91994A93C86F3B2F');
        $this->addSql('DROP TABLE Tasks');
        $this->addSql('DROP TABLE WorkWeeks');
    }
}
